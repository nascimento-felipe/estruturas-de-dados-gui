﻿namespace EstruturasDeDadosGUI
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsEstruturas = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemPilha = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemFila = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemLista = new System.Windows.Forms.ToolStripMenuItem();
            this.gpTitle = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsEstruturas});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsEstruturas
            // 
            this.tsEstruturas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemPilha,
            this.menuItemFila,
            this.menuItemLista});
            this.tsEstruturas.Name = "tsEstruturas";
            this.tsEstruturas.Size = new System.Drawing.Size(71, 20);
            this.tsEstruturas.Text = "Estruturas";
            // 
            // menuItemPilha
            // 
            this.menuItemPilha.Name = "menuItemPilha";
            this.menuItemPilha.Size = new System.Drawing.Size(100, 22);
            this.menuItemPilha.Text = "Pilha";
            this.menuItemPilha.Click += new System.EventHandler(this.pilhaToolStripMenuItem_Click);
            // 
            // menuItemFila
            // 
            this.menuItemFila.Name = "menuItemFila";
            this.menuItemFila.Size = new System.Drawing.Size(100, 22);
            this.menuItemFila.Text = "Fila";
            this.menuItemFila.Click += new System.EventHandler(this.menuItemFila_Click);
            // 
            // menuItemLista
            // 
            this.menuItemLista.Name = "menuItemLista";
            this.menuItemLista.Size = new System.Drawing.Size(100, 22);
            this.menuItemLista.Text = "Lista";
            this.menuItemLista.Click += new System.EventHandler(this.menuItemLista_Click);
            // 
            // gpTitle
            // 
            this.gpTitle.Location = new System.Drawing.Point(12, 27);
            this.gpTitle.Name = "gpTitle";
            this.gpTitle.Size = new System.Drawing.Size(776, 411);
            this.gpTitle.TabIndex = 1;
            this.gpTitle.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gpTitle);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Interface Gráfica de ESDC";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem tsEstruturas;
        private ToolStripMenuItem menuItemPilha;
        private ToolStripMenuItem menuItemFila;
        private ToolStripMenuItem menuItemLista;
        private GroupBox gpTitle;
    }
}